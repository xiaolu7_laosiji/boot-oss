package com.lagou.oss.config;

import com.aliyun.oss.OSSClient;
import com.lagou.oss.bean.AliyunOssBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    OSSClient ossClient(AliyunOssBean bean) {
        String endPoint = "http://" + bean.getEndPoint();
        return new OSSClient(endPoint, bean.getKey(),
                bean.getSecret());
    }
}
