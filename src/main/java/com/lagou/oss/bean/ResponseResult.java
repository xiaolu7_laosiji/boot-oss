package com.lagou.oss.bean;

import java.io.Serializable;

public class ResponseResult implements Serializable {
    private boolean success;
    private int code;
    private String message;
    private Object data;

    public ResponseResult(int code, String message) {
        this(code, message, null);
    }

    public ResponseResult(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public boolean isSuccess() {
        return ResultCode.SUCCESS.getCode() == code;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static ResponseResult ok(Object data) {
       return new ResponseResult(ResultCode.SUCCESS.getCode(), ResultCode.SUCCESS.getMessage(), data);
    }

    public static ResponseResult fail() {
        return new ResponseResult(ResultCode.FAILED.getCode(), ResultCode.FAILED.getMessage());
    }

    public static ResponseResult fail(int code, String message) {
        return new ResponseResult(code, message);
    }

    public static ResponseResult forbidden(Object data) {
        return new ResponseResult(ResultCode.FORBIDDEN.getCode(), ResultCode.FORBIDDEN.getMessage(), data);
    }

    public static ResponseResult unauthorized(Object data) {
        return new ResponseResult(ResultCode.UNAUTHORIZED.getCode(), ResultCode.UNAUTHORIZED.getMessage(), data);
    }
}