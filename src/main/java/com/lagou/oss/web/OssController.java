package com.lagou.oss.web;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.DownloadFileRequest;
import com.github.tobato.fastdfs.domain.StorePath;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import com.lagou.oss.bean.AliyunOssBean;
import com.lagou.oss.bean.ResponseResult;
import com.lagou.oss.bean.ResultCode;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@RequestMapping("/oss")
public class OssController {

    @Autowired
    private OSSClient ossClient;

    @Autowired
    private AliyunOssBean ossBean;

    @Autowired(required = false)
    private FastFileStorageClient fastFileStorageClient;

    /**
     *
     * @param key douban_1000432.jpg,douban_1000134.jpg
     * @return
     * @throws Throwable
     */
    @RequestMapping("/download")
    public ResponseEntity download(String key) {
        InputStream stream = this.ossClient.getObject(ossBean.getBucketName(), key).getObjectContent();
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.IMAGE_PNG)
                .body(new InputStreamResource(stream));
    }

    /**
     *
     * @param key douban_12235929.jpg
     * @return
     */
    @RequestMapping("/delete")
    public Object delete(String key) {
        this.ossClient.deleteObject(ossBean.getBucketName(), key);
        return ResponseResult.ok("");
    }

    @RequestMapping("/upload")
    public Object upload(MultipartFile file) throws Exception {
        int len  = file.getBytes().length;
        int max = 5 * 1024;
        if (len > max) {
            return ResponseResult.fail(ResultCode.VALIDATE_FAILED.getCode(), "文件过大");
        }
        String fileName = file.getOriginalFilename();
        String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
        String[] arr = new String[]{"jpg", "png", "jpeg"};
        if (!StringUtils.equalsAnyIgnoreCase(ext, arr)) {
            return ResponseResult.fail(ResultCode.VALIDATE_FAILED.getCode(), "扩展名不正确");
        }
        String key = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmm"))
                + RandomStringUtils.randomNumeric(6) + "." + ext;

        //https://buybuybuy4j.oss-cn-shenzhen.aliyuncs.com/douban_12235929.jpg
        String data = "http://" + ossBean.getBucketName() + "." + ossBean.getEndPoint() + "/" + key;
        this.ossClient.putObject(ossBean.getBucketName(), key, file.getInputStream());
        return ResponseResult.ok(data);
    }

    /**
     * fdfs的upload
     * @param file
     * @return
     * @throws Exception
     */
    @RequestMapping("/uploadFdfs")
    public Object fdfsUpload(MultipartFile file) throws Exception {
        int len  = file.getBytes().length;
        int max = 5 * 1024 * 1024;
        if (len > max) {
            return ResponseResult.fail(ResultCode.VALIDATE_FAILED.getCode(), "文件过大");
        }
        String fileName = file.getOriginalFilename();
        String ext = fileName.substring(fileName.lastIndexOf(".") + 1);
        String[] arr = new String[]{"jpg", "png", "jpeg"};
        if (!StringUtils.equalsAnyIgnoreCase(ext, arr)) {
            return ResponseResult.fail(ResultCode.VALIDATE_FAILED.getCode(), "扩展名不正确");
        }
        System.out.println("2");

        //https://buybuybuy4j.oss-cn-shenzhen.aliyuncs.com/douban_12235929.jpg
        StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(),len,ext,null);
        System.out.println(storePath.getGroup() + ":" + storePath.getPath() + ":"+storePath.getFullPath());
        return  storePath.getFullPath();
    }

}
