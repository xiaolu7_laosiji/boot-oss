# fastdfs安装

## 1.安装编译环境

yum install git gcc gcc-c++ make automake vim wget libevent -y

## 2.安装基础库

```shell
cd /opt/soft/fdfs
git clone https://github.com/happyfish100/libfastcommon.git --depth 1 

cd libfastcommon/

./make.sh && ./make.sh install
```

## 3.安装fastdfs

```shell
cd /opt/soft/fdfs
wget https://github.com/happyfish100/fastdfs/archive/V5.11.tar.gz
tar -zxvf V5.11.tar.gz
cd fastdfs-5.11
./make.sh && ./make.sh install
#配置文件准备
cp /etc/fdfs/tracker.conf.sample /etc/fdfs/tracker.conf
cp /etc/fdfs/storage.conf.sample /etc/fdfs/storage.conf
cp /etc/fdfs/client.conf.sample /etc/fdfs/client.conf
cp /opt/soft/fdfs/fastdfs-5.11/conf/http.conf /etc/fdfs
cp /opt/soft/fdfs/fastdfs-5.11/conf/mime.types /etc/fdfs
```

执行完上面步骤后，使用**ls -lh /etc/fdfs** 验证

```
-rw-r--r--. 1 root root 1.5K 6月  30 12:49 client.conf
-rw-r--r--. 1 root root 1.5K 6月  30 12:49 client.conf.sample
-rw-r--r--. 1 root root  955 6月  30 12:50 http.conf
-rw-r--r--. 1 root root  31K 6月  30 12:51 mime.types
-rw-r--r--. 1 root root 3.7K 6月  30 13:00 mod_fastdfs.conf
-rw-r--r--. 1 root root 7.8K 6月  30 12:55 storage.conf
-rw-r--r--. 1 root root 7.8K 6月  30 12:49 storage.conf.sample
-rw-r--r--. 1 root root  105 6月  30 12:49 storage_ids.conf.sample
-rw-r--r--. 1 root root 7.3K 6月  30 12:52 tracker.conf
-rw-r--r--. 1 root root 7.3K 6月  30 12:49 tracker.conf.sample
```

**vi /etc/fdfs/tracker.conf**

```
#需要修改的内容如下
port=22122
base_path=/opt/soft/fdfs/data

```

**vi /etc/fdfs/storage.conf**

```
port=23000
base_path=/opt/soft/fdfs/data # 数据和日志文件存储根目录
store_path0=/opt/soft/fdfs/data # 第一个存储目录
tracker_server=192.168.3.24:22122
# http访问文件的端口(默认8888,看情况修改,和nginx中保持一致)
http.server_port=8888
subdir_count_per_path=32

```

和老师的有点不同，我调整了subdir配置，有利于存储海量小文件。

**启动**

```shell
/usr/bin/fdfs_trackerd /etc/fdfs/tracker.conf restart 

/usr/bin/fdfs_storaged /etc/fdfs/storage.conf restart
```

使用**netstat -ntlp**验证一下

```
tcp        0      0 0.0.0.0:22122           0.0.0.0:*               LISTEN      10581/fdfs_trackerd 
tcp        0      0 0.0.0.0:50090           0.0.0.0:*               LISTEN      27366/java          
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      20577/nginx: master 
tcp        0      0 0.0.0.0:50070           0.0.0.0:*               LISTEN      27172/java          
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      882/sshd            
tcp        0      0 0.0.0.0:8888            0.0.0.0:*               LISTEN      20577/nginx: master 
tcp        0      0 0.0.0.0:23000           0.0.0.0:*               LISTEN      10603/fdfs_storaged 
```

使用**/usr/bin/fdfs_monitor /etc/fdfs/storage.conf** 验证一下

```
DEBUG - base_path=/opt/soft/fdfs/data, connect_timeout=30, network_timeout=60, tracker_server_count=1, anti_steal_token=0, anti_steal_secret_key length=0, use_connection_pool=0, g_connection_pool_max_idle_time=3600s, use_storage_id=0, storage server id count: 0

server_count=1, server_index=0

tracker server is 192.168.3.24:22122

group count: 1

Group 1:
group name = group1
disk total space = 16570 MB
disk free space = 2808 MB
trunk free space = 0 MB
storage server count = 1
active server count = 1
storage server port = 23000
storage HTTP port = 8888
store path count = 1
subdir count per path = 32
current write server index = 0
current trunk file id = 0

        Storage 1:
                id = 192.168.3.24
                ip_addr = 192.168.3.24 (anantes-651-1-49-net.w2-0.abo.wanadoo.fr)  ACTIVE
                
```

使用**tree /opt/soft/fdfs/data**验证一下（只截取了一部分）

```
│   │   ├── 1C
│   │   ├── 1D
│   │   ├── 1E
│   │   └── 1F
│   ├── 10
│   │   ├── 00
│   │   ├── 01
│   │   ├── 02
│   │   ├── 03
│   │   ├── 04
│   │   ├── 05
│   │   ├── 06
│   │   ├── 07
│   │   ├── 08
│   │   ├── 09
│   │   ├── 0A
│   │   ├── 0B
│   │   ├── 0C
│   │   ├── 0D
│   │   ├── 0E
│   │   ├── 0F
│   │   ├── 10
│   │   ├── 11
│   │   ├── 12
│   │   ├── 13
│   │   ├── 14
│   │   ├── 15
│   │   ├── 16
│   │   ├── 17
│   │   ├── 18
│   │   ├── 19
│   │   ├── 1A
│   │   ├── 1B
│   │   ├── 1C
│   │   ├── 1D
│   │   ├── 1E
│   │   └── 1F
```

**测试上传**

忽略，请使用postman调用以下地址测试~

http://127.0.0.1:8080/oss/uploadFdfs

## 4.安装fastdfs-nginx-module

```shell
cd /opt/soft/fdfs
wget https://github.com/happyfish100/fastdfs-nginx-module/archive/V1.20.tar.gz
#解压
tar -xvf V1.20.tar.gz
cd fastdfs-nginx-module-1.20/src
vim config
#修改第5 行 和 15 行 修改成
ngx_module_incs="/usr/include/fastdfs /usr/include/fastcommon/"
CORE_INCS="$CORE_INCS /usr/include/fastdfs /usr/include/fastcommon/"
cp mod_fastdfs.conf /etc/fdfs/
```

```shell
vim /etc/fdfs/mod_fastdfs.conf
#需要修改的内容如下
tracker_server=192.168.3.24:22122
url_have_group_name=true
store_path0=/opt/soft/fdfs/data
mkdir -p /var/temp/nginx/client
```

## 5.重新编译nginx

```shell
cd /opt/soft/nginx/nginx-1.17.8
yum -y install pcre-devel openssl openssl-devel
# 添加fastdfs-nginx-module模块
./configure --add-module=/opt/soft/fdfs/fastdfs-nginx-module-1.20/src
make
#最好将原先的nginx备份一下
cp /opt/soft/nginx/nginx-1.17.8/objs/nginx /usr/local/nginx/sbin
```
**vim /usr/local/nginx/conf/nginx.conf**

```
server {
listen 8888;
server_name localhost;
location ~/group[0-9]/ {
ngx_fastdfs_module;
}
}
```

**/usr/local/nginx/sbin/nginx** 启动nginx

使用浏览器验证，前提是已经通过spring进行了文件上传：

http://192.168.3.24:8888/group1/M00/00/00/wKgDGF760rOASC5xAAHYBvzCyig648.png

截图如下：

![](image\fdfs.png)

## 6.安装GraphicsMagick

参考https://github.com/ahaii/Nginx-Lua-FastDFS-GraphicsMagick  
https://www.cnblogs.com/sunmmi/articles/6208181.html

**安装lua环境**

```shell
cd /opt/soft/fdfs

tar -xzvf /opt/soft/fdfs/LuaJIT-2.0.2.tar.gz

cd LuaJIT-2.0.2
make && make install
```

**vi /etc/profile**

修改一下环境变量

```
export LUAJIT_LIB=/usr/local/lib
export LUAJIT_INC=/usr/local/include/luajit-2.0
```

**准备lua模块**

lua-nginx-module-0.10.15

ngx_devel_kit-0.3.1

**重新编译nginx**

```shell
cd /opt/soft/nginx/nginx-1.17.8
./configure --add-module=lua-nginx-module-0.10.15 --add-module=ngx_devel_kit-0.3.1 --add-module=/opt/soft/fdfs/fastdfs-nginx-module-1.20/src
make
cp /opt/soft/nginx/nginx-1.17.8/objs/nginx /usr/local/nginx/sbin
```

**测试**

**vi /usr/local/nginx/conf/nginx.conf**

添加以下内容

```
location /test {
    default_type text/html;
    content_by_lua '
        ngx.say("hello world")
        ngx.log(ngx.ERR,"err err")
    ';
}
```

执行**/usr/local/nginx/sbin/nginx -s reload**

访问http://192.168.3.24/test，输出以下内容：

```
hello world
```

**开始安装**

```shell
cd /opt/soft/fdfs
tar zxvf GraphicsMagick-1.3.21.tar.gz
cd GraphicsMagick
./configure 
make && make install
```

完成上面步骤后，可以配置下lua，还有nginx.conf

**vi /usr/local/nginx/conf/nginx.conf**

参考以下配置文件

```
http {
    lua_package_path "/usr/local/nginx/lua/restyfastdfs.lua";
    server {
     listen 8888;
     server_name localhost;
     #location ~/group[0-9]/ {
     #ngx_fastdfs_module;
     #}
     location /group1/M00 {
		alias /opt/soft/fdfs/data/data;

		set $image_root "/opt/soft/fdfs/data/data";
		if ($uri ~ "/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/([a-zA-Z0-9]+)/(.*)") {
		set $image_dir "$image_root/$3/$4/";
			set $image_name "$5";
		set $file "$image_dir$image_name";
	    }

		  if (!-f $file) {
			content_by_lua_file "/usr/local/nginx/lua/fdfs.lua";
		  }
	   }
   }
}
```

lua文件，还有nginx.conf文件，都提交到了git上。

## 7.注意事项

安装GraphicsMagick之前，一定要执行：

**yum install -y libpng-devel libpng**

执行完上面命令后，重新编译安装：

```shell
cd /opt/soft/fdfs
cd GraphicsMagick
./configure 
make && make install
```

然后，还要提升fdfs数据目录的权限：

**chmod -R 777 /opt/soft/fdfs/data/data**

# fastdfs集群(完全版)

在192.168.3.7,192.168.3.8另外两台主机上，也安装上fastdfs，执行以下命令

```shell
yum install git gcc gcc-c++ make automake vim wget libevent -y
cd /opt/soft/fdfs
scp root@192.168.3.24:/opt/soft/fdfs/libfastcommon . 
cd libfastcommon/
./make.sh && ./make.sh install
cd /opt/soft/fdfs
scp root@192.168.3.24:/opt/soft/fdfs/V5.11.tar.gz . 
tar -zxvf V5.11.tar.gz
cd fastdfs-5.11
./make.sh && ./make.sh install
#配置文件准备
cp /etc/fdfs/tracker.conf.sample /etc/fdfs/tracker.conf
cp /etc/fdfs/storage.conf.sample /etc/fdfs/storage.conf
cp /etc/fdfs/client.conf.sample /etc/fdfs/client.conf
cp /opt/soft/fdfs/fastdfs-5.11/conf/http.conf /etc/fdfs
cp /opt/soft/fdfs/fastdfs-5.11/conf/mime.types /etc/fdfs

```

确保上面步骤完成后，编辑配置文件：

**vi /etc/fdfs/tracker.conf**

```
#需要修改的内容如下
port=22122
base_path=/opt/soft/fdfs/data
store_lookup=0 # 0是轮询，1是指定组,2是剩余存储空间多的group优先

```

**vi /etc/fdfs/storage.conf**

```
port=23000
base_path=/opt/soft/fdfs/data # 数据和日志文件存储根目录
store_path0=/opt/soft/fdfs/data # 第一个存储目录
tracker_server=192.168.3.7:22122
tracker_server=192.168.3.8:22122
http.server_port=8888
subdir_count_per_path=32
group_name=group1 #注意组名 
port=23000 #storage 的端口号,同一个组的 storage 端口号必须相同
```

**启动**

```shell
/usr/bin/fdfs_trackerd /etc/fdfs/tracker.conf restart 

/usr/bin/fdfs_storaged /etc/fdfs/storage.conf restart
```

使用/usr/bin/fdfs_monitor /etc/fdfs/storage.conf验证，输出以下内容：

![](\image\monitor.png)

## 安装fastdfs-nginx-module

确保上面步骤完成后，继续。

```shell
cd /opt/soft/fdfs
scp root@192.168.3.24:/opt/soft/fdfs/fastdfs-nginx-module-1.20 . 
cp mod_fastdfs.conf /etc/fdfs/
```

```shell
vim /etc/fdfs/mod_fastdfs.conf
#需要修改的内容如下
tracker_server=192.168.3.7:22122 #注意192.168.3.8(另一台机)这里要修改
url_have_group_name=true
store_path0=/opt/soft/fdfs/data
mkdir -p /var/temp/nginx/
```

## 安装nginx

```shell
cd /opt/soft/nginx/nginx-1.17.8
yum -y install pcre-devel openssl openssl-devel
# 添加fastdfs-nginx-module模块
./configure --add-module=/opt/soft/fdfs/fastdfs-nginx-module-1.20/src
make && make install
```

之后，我们可以配置以下两台机的nginx.conf

192.168.3.7的nginx.conf如下：

```
http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;
    server {
     listen 8888;
     server_name localhost;
     location ~/group[0-9]/ {
     ngx_fastdfs_module;
     }
    }
    upstream app{
          server 192.168.3.7:8888;
          server 192.168.3.8:8888;
    }

    #gzip  on;

    server {
        listen       80;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
          proxy_pass http://app/;
        }
      }
      }
```

192.168.3.8的nginx.conf如下：

```
http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;
    server {
     listen 8888;
     server_name localhost;
     location ~/group[0-9]/ {
     ngx_fastdfs_module;
     }
    }
```

然后，访问http://192.168.3.7/group1/M00/00/00/wKgDB18AXzeAYdkCAABFKo-iNhg171.png

进行一下测试，多刷新几次，发现图片显示正常，完成了基于nginx的fastdfs负载均衡。